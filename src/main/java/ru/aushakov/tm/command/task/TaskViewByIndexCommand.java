package ru.aushakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.model.Task;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskViewByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.TASK_VIEW_BY_INDEX;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "View task by index";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Task task = Optional
                .ofNullable(serviceLocator.getTaskService().findOneByIndex(index, userId))
                .orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

}
