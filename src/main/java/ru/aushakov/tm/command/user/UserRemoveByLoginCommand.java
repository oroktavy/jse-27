package ru.aushakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.command.AbstractUserCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.exception.entity.UserNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.USER_REMOVE_BY_LOGIN;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove user by login";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER USER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getUserService().removeOneByLogin(login))
                .orElseThrow(UserNotFoundException::new);
    }

}
