package ru.aushakov.tm.api.service;

import ru.aushakov.tm.dto.Domain;

public interface IDataService {

    String getFileBinary();

    String getFileBase64();

    Domain getDomain();

    void setDomain(Domain domain);

}
