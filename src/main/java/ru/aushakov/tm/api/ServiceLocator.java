package ru.aushakov.tm.api;

import ru.aushakov.tm.api.service.*;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

    IDataService getDataService();

}
