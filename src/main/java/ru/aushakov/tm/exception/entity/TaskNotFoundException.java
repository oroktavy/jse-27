package ru.aushakov.tm.exception.entity;

public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException() {
        super("Task not found!");
    }

}
