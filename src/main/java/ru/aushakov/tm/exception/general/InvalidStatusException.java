package ru.aushakov.tm.exception.general;

import org.jetbrains.annotations.Nullable;

public class InvalidStatusException extends RuntimeException {

    public InvalidStatusException() {
        super("Status is invalid!");
    }

    public InvalidStatusException(@Nullable final String statusId) {
        super("The value '" + statusId + "' entered is not a valid status!");
    }

}
