package ru.aushakov.tm.exception.empty;

public class EmptyDataListException extends RuntimeException {

    public EmptyDataListException() {
        super("Empty data list provided!");
    }

}
