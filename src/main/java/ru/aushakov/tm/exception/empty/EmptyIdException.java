package ru.aushakov.tm.exception.empty;

public class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Provided id is empty!");
    }

}
