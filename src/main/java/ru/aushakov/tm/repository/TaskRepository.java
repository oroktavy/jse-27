package ru.aushakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.model.Task;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    @Nullable
    public Task assignTaskToProject(
            @NotNull final String taskId,
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        @Nullable final Task task = findOneById(taskId, userId);
        Optional.ofNullable(task).ifPresent(t -> t.setProjectId(projectId));
        return task;
    }

    @Override
    @Nullable
    public Task unbindTaskFromProject(@NotNull final String taskId, @NotNull final String userId) {
        @Nullable final Task task = findOneById(taskId, userId);
        Optional.ofNullable(task).ifPresent(t -> t.setProjectId(null));
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAllTasksByProjectId(@NotNull final String projectId, @NotNull final String userId) {
        return list.stream()
                .filter(t -> projectId.equals(t.getProjectId()) && userId.equals(t.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<Task> removeAllTasksByProjectId(@NotNull final String projectId, @NotNull final String userId) {
        @NotNull final List<Task> deletedTasks = list.stream()
                .filter(t -> projectId.equals(t.getProjectId()) && userId.equals(t.getUserId()))
                .collect(Collectors.toList());
        list.removeAll(deletedTasks);
        return deletedTasks;
    }

}
