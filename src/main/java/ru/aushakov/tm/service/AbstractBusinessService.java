package ru.aushakov.tm.service;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.IBusinessRepository;
import ru.aushakov.tm.api.service.IBusinessService;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.empty.EmptyDescriptionException;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.empty.EmptyNameException;
import ru.aushakov.tm.exception.general.*;
import ru.aushakov.tm.model.AbstractBusinessEntity;
import ru.aushakov.tm.util.NumberUtil;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    @NonNull
    protected IBusinessRepository<E> businessRepository;

    @NonNull
    protected Class<E> currentClass;

    protected AbstractBusinessService(@NotNull IBusinessRepository<E> repository, @NotNull Class<E> currentClass) {
        this.repository = repository;
        this.businessRepository = repository;
        this.currentClass = currentClass;
    }

    @NotNull
    private E getNewEntity() {
        try {
            return currentClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @NotNull
    public List<E> findAll(@Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        return businessRepository.findAll(userId);
    }

    @Override
    @NotNull
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        return businessRepository.findAll(
                userId,
                Optional.ofNullable(comparator).orElseThrow(NoComparatorProvidedException::new)
        );
    }

    @Override
    @NotNull
    public E add(@Nullable final String name, @Nullable final String description, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        if (StringUtils.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final E entity = getNewEntity();
        entity.setUserId(userId);
        entity.setName(name);
        entity.setDescription(description);
        businessRepository.add(entity);
        return entity;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        businessRepository.clear(userId);
    }

    @Override
    @Nullable
    public E findOneById(@Nullable final String id, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return businessRepository.findOneById(id, userId);
    }

    @Override
    @Nullable
    public E findOneByIndex(@NotNull final Integer index, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return businessRepository.findOneByIndex(index, userId);
    }

    @Override
    @Nullable
    public E findOneByName(@Nullable final String name, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.findOneByName(name, userId);
    }

    @Override
    @Nullable
    public E removeOneById(@Nullable final String id, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return businessRepository.removeOneById(id, userId);
    }

    @Override
    @Nullable
    public E removeOneByIndex(@NotNull final Integer index, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return businessRepository.removeOneByIndex(index, userId);
    }

    @Override
    @Nullable
    public E removeOneByName(@Nullable final String name, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.removeOneByName(name, userId);
    }

    @Override
    @Nullable
    public E updateOneById(
            @Nullable final String id,
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.updateOneById(id, userId, name, description);
    }

    @Override
    @Nullable
    public E updateOneByIndex(
            @NotNull final Integer index,
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.updateOneByIndex(index, userId, name, description);
    }

    @Override
    @Nullable
    public E startOneById(@Nullable final String id, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return businessRepository.startOneById(id, userId);
    }

    @Override
    @Nullable
    public E startOneByIndex(@NotNull final Integer index, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return businessRepository.startOneByIndex(index, userId);
    }

    @Override
    @Nullable
    public E startOneByName(@Nullable final String name, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.startOneByName(name, userId);
    }

    @Override
    @Nullable
    public E finishOneById(@Nullable final String id, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return businessRepository.finishOneById(id, userId);
    }

    @Override
    @Nullable
    public E finishOneByIndex(@NotNull final Integer index, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return businessRepository.finishOneByIndex(index, userId);
    }

    @Override
    @Nullable
    public E finishOneByName(@Nullable final String name, @Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return businessRepository.finishOneByName(name, userId);
    }

    @Override
    @Nullable
    public E changeOneStatusById(
            @Nullable final String id,
            @Nullable final Status status,
            @Nullable final String userId
    ) {
        throw new NotSupportedOnServiceLayerException("changeOneStatusById");
    }

    @Override
    @Nullable
    public E changeOneStatusByIndex(
            @NotNull final Integer index,
            @Nullable final Status status,
            @Nullable final String userId
    ) {
        throw new NotSupportedOnServiceLayerException("changeOneStatusByIndex");
    }

    @Override
    @Nullable
    public E changeOneStatusByName(
            @Nullable final String name,
            @Nullable final Status status,
            @Nullable final String userId
    ) {
        throw new NotSupportedOnServiceLayerException("changeOneStatusByName");
    }

    @Override
    @Nullable
    public E changeOneStatusById(
            @Nullable final String id,
            @Nullable final String statusId,
            @Nullable final String userId
    ) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        @NotNull final Status status = Optional
                .ofNullable(Status.toStatus(statusId)).orElseThrow(InvalidStatusException::new);
        return businessRepository.changeOneStatusById(id, status, userId);
    }

    @Override
    @Nullable
    public E changeOneStatusByIndex(
            @NotNull final Integer index,
            @Nullable final String statusId,
            @Nullable final String userId
    ) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        @NotNull final Status status = Optional
                .ofNullable(Status.toStatus(statusId)).orElseThrow(InvalidStatusException::new);
        return businessRepository.changeOneStatusByIndex(index, status, userId);
    }

    @Override
    @Nullable
    public E changeOneStatusByName(
            @Nullable final String name,
            @Nullable final String statusId,
            @Nullable final String userId
    ) {
        if (StringUtils.isEmpty(userId)) throw new NoUserLoggedInException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Status status = Optional
                .ofNullable(Status.toStatus(statusId)).orElseThrow(InvalidStatusException::new);
        return businessRepository.changeOneStatusByName(name, status, userId);
    }

}
